# TheMatrix #

'TheMatrix' is a scalable fault-tolerant key value store which was implemented using **Docker** and **Flask**. This was done as a 4 part assignment for CMPS 128 - Winter 2017 course at UCSC. A brief description about the 4 assignments can be found below:
   
### Assignment 1 ###

Introduction to Docker images and Web App service. We decided to use Flask.

### Assignment 2 ###

Create a main node and forwarder node. Implement GET PUT and DELETE for your key value store.

### Assignment 3 ###

Make the key value store scalable. Data gets partitioned across various nodes (docker images).

### Assignment 4 ###

Make the key value store fault-tolerant. Data is replicated into various nodes. Our key value store is then tested by creating a network partition and then healing it back. The key value store should be eventually consistent.  
 
In depth requirements for every assignment can be found [here](https://github.com/palvaro/CMPS128-Winter17).