import os
from flask_restful import reqparse
from flask import Flask, request
from flask import jsonify
import hashlib
import requests as req
import time
from apscheduler.scheduler import Scheduler
import json
import atexit

import logging
logging.basicConfig()

cron = Scheduler(daemon = True)
# Explicitly kick off the background thread
cron.start()

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

#dictionaries: 
#kvs_store => {key : [val, timestamp, vector_clocl]}
#node_pos => {partitionid_hash : partition_id}
#part => {partition_id : [nodes]}

kvs_store = {}
node_pos = {}
part = {}
current_timestamp = 0

def get_hash(key):
    return int(hashlib.sha1(str(key).encode('utf-8')).hexdigest(), 32) % 10000

#Initialise vector clock with all 0s
def init_vector_clock():
    K = int(os.environ.get("K"))
    vector_clock = "0." * K
    return vector_clock[:len(vector_clock) - 1]

#Increament vector clock at position pos
def incr_vector_clock(vector_str):
    pos = get_node_pos_in_part()
    vector_cl_list = map(int, vector_str.split('.'))
    vector_cl_list[pos] += 1
    return ".".join(map(str, vector_cl_list))

def get_node_pos_in_part():
    partition_id = os.environ.get("PARTITION_ID")
    node_ip = os.environ.get("IPPORT")
    return part[int(partition_id)].index(node_ip)

def find_max_vector_clock(sent_vector_clock,vector_clock,key):
    node_position = get_node_pos_in_part()
    vector_cl_list = map(int, vector_clock.split('.'))
    value1=map(int ,sent_vector_clock.split('.'))
    value=value1[node_position]
    max=0
    if vector_cl_list[node_position]<value:
      vector_cl_list[node_position]=value
      max=1
    if max==0: 
       vector_clock = kvs_store[key][1]
       updated_vector_clock = incr_vector_clock(vector_clock)
       kvs_store[key][1]=updated_vector_clock
    else:
       #vector_cl_list =map(int,vector_str.split('.'))
       vector_cl_list[node_position]=value
       kvs_store[key][1]=".".join(map(str, vector_cl_list))

    return max 

def init_dict():
    if "VIEW" in os.environ:
        K = int(os.environ.get("K"))
        own_ip = os.environ.get("IPPORT").replace(" ", "")
        str_ips = os.environ.get("VIEW").replace(" ", "")
        list_ips = str_ips.split(",")
        partition_id = 1
        ind = 0
        while ind < len(list_ips):
            if ind + K > len(list_ips):
                part[int(partition_id)] = list_ips[ind :]
            else:
                part[int(partition_id)] = list_ips[ind : ind + K]
            if own_ip in part[partition_id]:
                os.environ["PARTITION_ID"] = str(partition_id)
            ind += K
            partition_id += 1

        #Initialise part dictionary
        partitions = sorted(part.keys())
        for partition in partitions:
            node_pos[(get_hash(partition))] = partition



#This function is returning partition_id to which the hash value belongs
def get_node(key_hash):
	node_pos_list = sorted(node_pos.keys())
	node_index = 0
	for index in range(len(node_pos_list)):
		if node_pos_list[index] > key_hash:
			node_index = index
			break
	return node_pos[node_pos_list[node_index]]

def forward_delete_key(key):
    partition_nodes = part[int(os.environ.get('PARTITION_ID'))]
    for node in partition_nodes:
        if node not in os.environ.get('PARTITION_ID'):
            address = node
            ur_str = 'http://' + address + '/kvs/delete_key/' + key
            req.put(ur_str)

@app.route("/kvs/delete_key/<key>", methods = ['GET'])
def delete_key(key):
    if key in kvs_store.keys():
        del kvs_store[key]


@app.route("/kvs/redistribute_keys", methods = ['GET'])
def redistribute_keys():
    req_type = request.args.get('type')
    neighbor_id = int(request.args.get('neighbor'))
    if req_type in "add":
        for key in kvs_store.keys():
            key_hash = get_hash(key)
            partition_id = get_node(key_hash)
            address = part[neighbor_id][0]
            if str(partition_id) not in os.environ.get('PARTITION_ID'):
                ur_str = 'http://' + address + '/kvs/' + key
                data = {'val' : kvs_store[key][0], 'causal_payload' : kvs_store[key][1], "timestamp" : kvs_store[key][2]}
                result = req.put(ur_str, data)
                if result.status_code is 200:
                    del kvs_store[key]
                else:
                    index = 1
                    while result.status_code != 200 and index < os.environ.get('K'):
                        address = part[partition_id][index]
                        index += 1
                        ur_str = 'http://' + address + '/kvs/' + key
                        result = req.put(ur_str, data)
                forward_delete_key(key)


    if req_type in "remove":
        address = part[neighbor_id][0]
        for key in kvs_store.keys():
            ur_str = 'http://' + address + '/kvs/' + key
            data = {'val' : kvs_store[key][0], 'causal_payload' : kvs_store[key][1], "timestamp" : kvs_store[key][2]}
            result = req.put(ur_str, data)
            index = 1
            while result.status_code != 200 and os.environ.get('K'):
                address = part[partition_id][index]
                index += 1
                ur_str = 'http://' + address + '/kvs/' + key
                result = req.put(ur_str, data)


    return jsonify(msg = ('success'))


def get_neighbor(hash_val):
	# Update own node_position dictionary
	node_hash_list = sorted(node_pos.keys())  #To do: change dict name
	partition_node_index = 0
	for index in range(len(node_hash_list)):
		if node_hash_list[index] > hash_val:
			partition_node_index = index
			break

	return node_pos[node_hash_list[partition_node_index]]


def forward_redistribute_req(partition_id, broadcast_type, neighbor_id):
	#Request 0th node of partition to redistribute your keys
        address = part[partition_id][0]
	ur_str = 'http://' + address + '/kvs' + '/redistribute_keys'
        params = {'type' : broadcast_type, 'neighbor' : neighbor_id}
	result = req.get(ur_str, params=params)

        #if didn't get anything, resend request to next nodes
        index = 1
        while(result.status_code != 200 and index < int(os.environ.get("K"))):
            address = part[partition_id][index]
            index += 1
            ur_str = 'http://' + address + '/kvs' + '/redistribute_keys'
            params = {'type' : broadcast_type}
            result = req.get(ur_str, params=params)


def get_vacant_partition_id():
    part_size = int(os.environ.get("K"))
    for partition_id in part:
        if len(part[partition_id]) < part_size:
            return partition_id

    return -1


def update_env_var(ip_port, partition_id):
    ur_str = 'http://' + ip_port + '/kvs' + '/update_env_part'
    data = {'partition_id': partition_id}
    req.post(ur_str, data)


@app.route("/kvs/update_env_part", methods=['PUT', 'POST'])
def update_env_part():
   os.environ["PARTITION_ID"] = request.form.get('partition_id')
   return jsonify(msg = ('success'))

@app.route("/kvs/delete_kvs", methods = ['GET'])
def delete_kvs():
    global kvs_store
    kvs_store = {}
    return jsonify(msg = ('success'))

def forward_delete_key_req(address):
    ur_str = 'http://' + address + '/kvs' + '/delete_kvs'
    req.get(ur_str)

@app.route("/kvs/update_view", methods=['PUT', 'POST'])
def view_update():
	arg_type = request.args.get('type')
	ip_port = request.form.get('ip_port')
        global current_timestamp
        current_timestamp = int(time.time())

	if "add" in arg_type:
                affected_partition = int(get_vacant_partition_id())
                updated_view = os.environ.get("VIEW") + "," + ip_port
		os.environ["VIEW"] = updated_view
		broadcast_type = 'add'
                if affected_partition != -1:
                    part[affected_partition] += [ip_port]
                    redistribute_partition = -1
                else:
                    node_pos[get_hash(len(part.keys()) + 1)] = len(part.keys()) + 1
                    part[len(part.keys()) + 1] = [ip_port]
                    affected_partition = len(part.keys()) 
                    redistribute_partition = get_neighbor(get_hash(affected_partition))
                update_env_var(ip_port, affected_partition)
                response = jsonify(msg = ("success"), partition_id = affected_partition, number_of_partitions = len(part))

	if "remove" in arg_type:
		broadcast_type = 'remove'
                for partition_id in part.keys():
                    if ip_port in part[partition_id]:
                        affected_partition = partition_id
                        break
                if affected_partition != sorted(part.keys())[-1]:
                    updated_view = os.environ.get('VIEW').replace(" ", '')
                    last_node = updated_view.split(',')[-1]
                    updated_view = updated_view.replace(last_node, "")
                    updated_view = updated_view.replace(ip_port, last_node)
                    updated_view = updated_view[:len(updated_view) - 1]
                    os.environ["VIEW"] = updated_view

                    last_part_id = sorted(part.keys())[-1]
                    if len(part[last_part_id]) == 1:
                        redistribute_partition = last_part_id
                        neighbor_partition = get_neighbor(get_hash(redistribute_partition))
                        forward_redistribute_req(redistribute_partition, broadcast_type, neighbor_partition)
                        #forward_delete_key_req(last_node)
                        del node_pos[get_hash(last_part_id)]
                        del part[last_part_id]
                    else:
                        #forward_delete_key_req(last_node)
                        part[last_part_id].remove(last_node)

                    part[affected_partition] += [last_node]
                    update_env_var(last_node, affected_partition)                       

                elif len(part[partition_id]) == 1:
                    updated_view = remove(ip_port, os.environ.get("VIEW"))
                    os.environ["VIEW"] = updated_view
                    redistribute_partition = partition_id
                    neighbor_partition = get_neighbor(get_hash(redistribute_partition))
                    forward_redistribute_req(redistribute_partition, broadcast_type, neighbor_partition)
                    del node_pos[get_hash(partition_id)]
                    del part[partition_id]
                else:
                    updated_view = remove(ip_port, os.environ.get("VIEW"))
                    os.environ["VIEW"] = updated_view
                    part[partition_id].remove(ip_port)
                    part[partition_id] = part[partition_id]

                response = jsonify(msg = ("success"), number_of_partitions = len(part))

	#Do we need to replicate?

        for x in os.environ.get("VIEW").split(','):
		if os.environ.get("IPPORT") not in x:
			# broadcast message to x
			ur_str = 'http://'+ x + '/'+'kvs/view_update_broadcast'
                        data = {'type' : broadcast_type, 'ip_port' : ip_port, 'view': updated_view, 'partition_id' : affected_partition}
			req.put(ur_str, data=data)

        if "add" in arg_type and redistribute_partition != -1:
            forward_redistribute_req(redistribute_partition, broadcast_type, affected_partition)
        response.status_code = 200
	return response


@app.route("/kvs/view_update_broadcast", methods=['PUT', 'POST'])
def view_update_broadcast():
    arg_type = request.form.get('type')
    view = request.form.get('view')
    ip_port = request.form.get('ip_port')
    partition_id = int(request.form.get('partition_id'))
    call_init_dict = False
    
    #Update part dictionary
    if request.method == 'PUT':
        if "add" in arg_type:
            if partition_id in part.keys():
                    part[int(partition_id)] += [ip_port]
            else:
                    part[int(partition_id)] = [ip_port]
                    node_pos[get_hash(partition_id)] = partition_id
                    call_init_dict = True

        elif "remove" in arg_type:
            if partition_id != sorted(part.keys())[-1]:
                last_part_id = sorted(part.keys())[-1]
                last_node = part[last_part_id][-1]
                if len(part[last_part_id]) == 1:
                    del node_pos[get_hash(last_part_id)]
                    del part[last_part_id]
                else:
                    part[last_part_id].remove(last_node)
               
                part[partition_id].remove(ip_port)
                part[partition_id] += [last_node]

            elif len(part[partition_id]) == 1 and ip_port in part[partition_id]:
                del part[partition_id]
                del node_pos[get_hash(partition_id)]
            elif ip_port in part[partition_id]:
                part[partition_id].remove(ip_port)
                part[partition_id] = part[partition_id]

    #Update VIEW environment variable
    os.environ["VIEW"] = view
    if call_init_dict:
        init_dict()

    return jsonify(msg = ('success'))


def remove(ip_port, view):
	result = ""
	for x in view.split(','):
			if ip_port not in x:
				result = result + x + ","
	result = result[:-1]
	return result

@app.route("/kvs/<key>", methods = ['GET'])
def getKey(key):
    temp = get_hash(key)
    keylist = node_pos.keys()
    partition_id = get_node(get_hash(key))
    result = {}
    if os.environ.get('PARTITION_ID') not in str(partition_id):
      index = 0
      while index < int(os.environ.get("K")):
        address = part[partition_id][index]
        url_string = 'http://' + address + '/' + 'kvs/' + key
        try:
          return (req.get(url_string)).content
          break
        except Exception as e:
          print url_string + " " + str(e)
          index = index + 1
      return "All nodes in the partition could not be reached", 500 
    else:
        if key not in kvs_store:
          response = jsonify(msg = ('error'), error=('key does not exist'))
        else:
          value=incr_vector_clock(kvs_store[key][1])
          kvs_store[key][1]=value
          response = jsonify(msg = ('success'), value = kvs_store[key][0], partition_id = os.environ.get("PARTITION_ID"), causal_payload = kvs_store[key][1], timestamp = kvs_store[key][2])
    return response

@app.route("/kvs/<key>", methods=['PUT','POST'])
def putKey(key):
    value = request.form.get('val')
    partition_id = get_node(get_hash(key))
    
    causal_payloadValue = request.form.get('causal_payload')
    data = {'val' : value ,'causal_payload' : causal_payloadValue}
    # result = {}
    if os.environ.get('PARTITION_ID') not in str(partition_id):
      index = 0
      while index < int(os.environ.get("K")):
        address = part[partition_id][index]
        url_string = 'http://' + address + '/' + 'kvs/' + key
        try:
          return (req.put(url_string, data)).content
          break
        except Exception as e:
          print url_string + " " + str(e)
          index = index + 1
      return "All nodes in the partition could not be reached", 500

    else:      
            if key in kvs_store.keys():
              
              if causal_payloadValue is not None and len(causal_payloadValue) > 0:
                find_max_vector_clock(causal_payloadValue,kvs_store[key][1],key)
                updated_vector_clock=kvs_store[key][1]
              else:
                  vector_clock = kvs_store[key][1]
                  updated_vector_clock = incr_vector_clock(vector_clock)
                  kvs_store[key][1]=updated_vector_clock
              time_stamp = int(time.time())
              curr_partition_id = (os.environ.get("PARTITION_ID"))
              kvs_store[key][0]=value
              kvs_store[key][2]=time_stamp
            else:
                if causal_payloadValue is None or len(causal_payloadValue) == 0:
                  vector_clock = init_vector_clock()
                  updated_vector_clock = incr_vector_clock(vector_clock)
                  
                else:
                  pos = get_node_pos_in_part()
                  causal_payloadValue_str = causal_payloadValue.encode('utf8')
                  vector_cl_list = map(int, causal_payloadValue_str.split('.'))
                  vector_cl_list[pos] =causal_payloadValue[pos]
                  updated_vector_clock=".".join(map(str, vector_cl_list))
                time_stamp = int(time.time())
                curr_partition_id = (os.environ.get("PARTITION_ID"))
                kvs_store[key] = [value, updated_vector_clock, time_stamp]
            return jsonify(msg = ('success'), partition_id = curr_partition_id, causal_payload = updated_vector_clock, timestamp = time_stamp)

#Obtaining Partition Information
@app.route("/kvs/get_partition_id", methods = ['GET'])
def get_partition_id():
    partition = os.environ.get("PARTITION_ID")
    return jsonify(msg = ('success'), partition_id = partition)

@app.route("/kvs/get_all_partition_ids", methods = ['GET'])
def get_all_partition_ids():
   partitions = sorted(part.keys())
   return jsonify(msg = ('success'), partition_id_list = partitions)


@app.route("/kvs/get_partition_members", methods = ['GET'])
def get_partition_members():
    partition_id = request.form.get('partition_id')
    if int(partition_id) in part.keys():
        members_list = part[int(partition_id)]
        return jsonify(msg = ('success'), partition_members = members_list)
    else:
        return jsonify(msg = ('error'), error = ('key value store is not available'))

@cron.interval_schedule(seconds = 0.2)
def replicate():
        with app.test_request_context():
            from flask import request
            import requests as req
            import flask
            partition_id = os.environ.get("PARTITION_ID")
            list_node_id = part[int(partition_id)]
            replicate_type = request.args.get('type')
            for values in list_node_id:
                    if values not in os.environ.get("IPPORT"):
                            url_string='http://'+ values + '/' + 'replicate'
                            
                            data = {'dictionary' : json.dumps([{'key': k, 'params' : p} for k, p in kvs_store.items()], indent = 4)}
                            result = req.post(url_string, data )
                            data = {'part_dictionary' : json.dumps([{'key':k,'params' :p} for k, p in part.items()],indent =4), 'node_dict' : json.dumps([{'key':k,'params' :p} for k, p in node_pos.items()],indent =4),'timestamp' :json.dumps(current_timestamp)}
                            url_stringView='http://'+ values + '/' + 'replicateView'                          
                            result= req.post(url_stringView, data )

# Shutdown your cron thread if the web process is stopped
atexit.register(lambda: cron.shutdown(wait=False))

@app.route("/replicate", methods = ['POST'])
def replicate_requests():
        jsondata2 = request.form.get('dictionary')
        jsondata = json.loads(jsondata2)
        
        if len(jsondata) > 0:
            for dictionary in jsondata:
                received_key = dictionary["key"]
                params = dictionary["params"]
                received_ts = params[2]
                if received_key not in kvs_store.keys():
                    kvs_store[received_key] = params
                elif params[2] > kvs_store[received_key][2]:
                     kvs_store[received_key] = params

                payload=map(int, params[1].encode('utf8').split('.'))
                self_payload=map(int, kvs_store[received_key][1].split('.'))
                counter=0
                if counter<len(payload):
                    if payload[counter]>self_payload[counter]:
                        self_payload[counter]=payload[counter]
                        counter=counter+1
                kvs_store[received_key][1]=".".join(map(str, self_payload))

        return jsonify(msg = ('success'))


@app.route("/replicateView", methods= ['POST'])
def replicate_views():
    part_dict = request.form.get('part_dictionary')
    json_part_dict = json.loads(part_dict)
    node_dict = request.form.get('node_dict')
    json_node_dict = json.loads(node_dict)

    timestamp = request.form.get('timestamp')
    json_timestamp = json.loads(timestamp)
    global current_timestamp
    
    '''if len(json_part_dict) > 0:
        for dictionary in json_part_dict:
            part_id = int(dictionary["key"])
            nodes  = dictionary["params"]
            part[int(part_id)] = nodes
    if len(json_node_dict) > 0:
        for dicitonary in json_node_dict:
            part_hash = int(dictionary["key"])
            partition = dictionary["params"]
            node_pos[part_hash] = partition'''
    return jsonify(msg = ('success'))

@app.route("/kvs/dummy/<key>", methods = ['POST', 'PUT'])
def dummy(key):
    val = request.form.get('val')
    pay = request.form.get('payload')
    return jsonify(val = val, pay = pay)

if __name__ == '__main__':
    	port = int(os.environ.get('PORT', 8080))
	init_dict()
	app.run(host='0.0.0.0',port=port, threaded=True)
        #app.run()
